# pylint:disable=protected-access
# pylint:disable=no-self-use
# pylint:disable=comparison-with-callable

from .....src.infra.utils.thread.threading_thread import ThreadingThread


class PythonThreadStub:
    """
    A Python Thread stub class
    """

    def __init__(self, target, args, daemon):
        self.target = target
        self.args = args
        self.daemon = daemon

    def start(self):
        """
        Start thread stub
        """

    def join(self, timeout):
        """
        Join thread stub
        """


class CallbackHolder:
    """
    Test the aspects of CallbackHolder class
    """

    def __init__(self, callback):
        self.callback = callback


def make_sut(thread=PythonThreadStub) -> ThreadingThread:
    """
    Creates the system under test object
    """
    return ThreadingThread(thread=thread)


class TestThreadingThread:
    """
    Test the aspects of ThreadingThread class
    """

    class TestBuild:
        """
        Test the aspects of the build method
        """

        def test_build_assing(self):
            """
            Ensure ThreadingThread correctly assigns atributes upon building
            """
            sut = make_sut()
            target = lambda: None
            sut.build(target=target)
            assert sut._target == target
            assert sut._args == ()
            assert sut._daemon is True
            assert sut._keep_alive is False

        def test_build_thread_assing(self):
            """
            Ensure ThreadingThread correctly assembles the Thread object
            """
            sut = make_sut()
            target = lambda: None
            args = [1, 2, 3]
            daemon = True
            sut.build(target=target, args=args, daemon=daemon)
            assert isinstance(sut._thread, PythonThreadStub)
            assert sut._thread.target == sut._keep_alive_wrapper
            assert list(sut._thread.args) == args
            assert sut._thread.daemon == daemon

    class TestStart:
        """
        Test the aspects of the start method
        """

        def test_start_call(self, mocker):
            """
            Ensure TestClass calls start once
            """
            thread = PythonThreadStub(lambda: None, [], True)
            start_spy = mocker.spy(thread, "start")
            sut = make_sut()
            sut._thread = thread
            sut.start()
            start_spy.assert_called_once()

    class TestJoin:
        """
        Test the aspects of the join method
        """

        def test_join_call(self, mocker):
            """
            Ensure TestClass calls join with correct value
            """
            thread = PythonThreadStub(lambda: None, [], True)
            join_spy = mocker.spy(thread, "join")
            sut = make_sut()
            sut._thread = thread
            sut.join()
            join_spy.assert_called_with(None)
            sut.join(1.3)
            join_spy.assert_called_with(1.3)

    class TestKeepAliveWrapper:
        """
        Test the aspects of the _keep_alive_wrapper method
        """

        def test_wrapper_target_call(self, mocker):
            """
            Ensure ThreadingThread wrapper calls target once
            """
            callback_holder = CallbackHolder(callback=lambda: None)
            target_spy = mocker.spy(callback_holder, "callback")
            sut = make_sut()
            sut.build(target=callback_holder.callback)
            sut._keep_alive_wrapper()
            target_spy.assert_called_once()
