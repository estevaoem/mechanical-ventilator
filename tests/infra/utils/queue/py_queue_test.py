# pylint:disable=no-self-use
# pylint:disable=arguments-differ

from queue import Queue as QueueInfra

from .....src.infra.utils.queue.py_queue import PyQueue


class QueueStub(QueueInfra):
    """
    A Queue Stub class
    """

    def put(self, item: dict) -> None:
        """
        Puts a dict item into the queue
        """

    def get(self) -> dict:
        """
        Gets a dict item from the queue
        """
        return dict(hello="world")


def make_sut(queue: QueueInfra = QueueStub()) -> PyQueue:
    """
    Creates the system under test object
    """
    return PyQueue(queue=queue)


class TestPyQueue:
    """
    Test the aspects of PyQueue class
    """

    def test_queue_put_call(self, mocker):
        """
        Ensure PyQueue calls queue put with correct value
        """
        queue = QueueStub()
        put_spy = mocker.spy(queue, "put")
        sut = make_sut(queue=queue)
        sut.put(dict(hello="world"))
        put_spy.assert_called_once_with(dict(hello="world"))

    def test_queue_call(self, mocker):
        """
        Ensure PyQueue calls queue get method once
        """
        queue = QueueStub()
        get_spy = mocker.spy(queue, "get")
        sut = make_sut(queue=queue)
        sut.get()
        get_spy.assert_called_once()

    def test_queue_return_on_success(self):
        """
        Ensure PyQueue returns data from queue get on success
        """
        sut = make_sut()
        data = sut.get()
        assert data == dict(hello="world")
