# pylint:disable=no-self-use
# pylint:disable=protected-access

import inspect

from .....src.common import env
from .....src.infra.data.serial_reader.py_serial_reader import (PySerialReader,
                                                                Serial)


class PySerialStub:
    """
    A PySerial Stub class
    """

    def __init__(self, port=None, baudrate=None):
        self.port = port
        self.baudrate = baudrate

    def readline(self) -> bytes:
        """
        Pretends to read a line on serial stream
        """
        return b"message\n"

    def write(self, message) -> None:
        """
        Pretends to write a line on serial stream
        """


def make_sut(pyserial=PySerialStub) -> PySerialReader:
    """
    Creates the system under test object
    """
    return PySerialReader(pyserial=pyserial)


class TestPySerialReader:
    """
    Test the aspects of PySerialReader class
    """

    def test_default_implementation_class(self):
        """
        Ensure PySerialReader has Serial class as default serial provider
        """
        sut = PySerialReader
        signature = inspect.signature(sut.__init__)
        assert signature.parameters["pyserial"].default == Serial

    class TestBuild:
        """
        Test the aspects of _build method
        """

        def test_build_assemble(self):
            """
            Ensure PySerialReader is correctly assembled
            """
            sut = make_sut()
            assert isinstance(sut._serial, PySerialStub)
            assert sut._serial.port == env["SERIAL_PORT"]
            assert sut._serial.baudrate == env["SERIAL_BAUDRATE"]

    class TestReadLine:
        """
        Test the aspects of read_line method
        """

        def test_read_line_call(self, mocker):
            """
            Ensure PySerialReader calls read_line with correct value
            """
            sut = make_sut()
            pyserial = PySerialStub()
            readline_spy = mocker.spy(pyserial, "readline")
            sut._serial = pyserial
            sut.read_line()
            readline_spy.assert_called_once()

        def test_return(self):
            """
            Ensure PySerialReader returns correct data
            """
            sut = make_sut()
            assert sut.read_line() == "message"

    class TestSendAck:
        """
        Test the aspects of read_line method
        """

        def test_write_call(self, mocker):
            """
            Ensure PySerialReader calls write with correct value
            """
            sut = make_sut()
            pyserial = PySerialStub()
            write_spy = mocker.spy(pyserial, "write")
            sut._serial = pyserial
            sut.send_ack("message")
            write_spy.assert_called_once_with(f"{str(dict(ack='message'))}\n".encode())
