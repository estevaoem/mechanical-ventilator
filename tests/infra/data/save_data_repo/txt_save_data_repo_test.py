# pylint:disable=no-self-use
import datetime

from .....src.infra.data.save_data_repo.txt_save_data_repo import \
    TxtSaveDataRepo


def make_sut() -> TxtSaveDataRepo:
    """
    Creates the system under test object
    """
    return TxtSaveDataRepo()


def after_all() -> None:
    """
    Function executed after all
    """
    with open("logs.txt", mode="w", encoding="utf-8") as file:
        file.write("")


class TestTxtSaveDataRepo:
    """
    Test the aspects of TxtSaveDataRepo class
    """

    def test_file_log(self):
        """
        Ensure TxtSaveDataRepo logs data to a txt file
        """
        sut = make_sut()
        sut.save("Hello World")
        with open("logs.txt", mode="r", encoding="utf-8") as file:
            contents = file.read()
            assert "Hello World" in contents
        after_all()

    def test_file_time_log(self):
        """
        Ensure TxtSaveDataRepo logs unixtime to a txt file
        """
        sut = make_sut()
        sut.save("Hello World")
        with open("logs.txt", mode="r", encoding="utf-8") as file:
            contents = file.read()

        try:
            unixtime = float(contents.split("\t")[0])
            date = datetime.datetime.fromtimestamp(unixtime)
            assert isinstance(date, datetime.datetime)
        except ValueError:
            assert False
        except TypeError:
            assert False
        finally:
            after_all()
