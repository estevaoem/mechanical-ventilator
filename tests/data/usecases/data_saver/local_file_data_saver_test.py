# pylint:disable=no-self-use

from .....src.data.protocols.save_data_repo import SaveDataRepo
from .....src.data.usecases.data_saver.local_file_data_saver import \
    LocalFileDataSaver
from .....src.utils.queue import Queue


class SaveDataRepoStub(SaveDataRepo):
    """
    A SaveDataRepo Stub class
    """


class QueueStub(Queue):
    """
    A Queue Stub class
    """

    def get(self) -> dict:
        """
        Gets a dict item from the queue
        """
        return dict(valid="data")

    def put(self, item: dict) -> None:
        """
        Puts a dict item into the queue
        """


def make_sut(save_data_repo: SaveDataRepo = SaveDataRepoStub()) -> LocalFileDataSaver:
    """
    Creates the system under test object
    """
    return LocalFileDataSaver(save_data_repo=save_data_repo)


class TestLocalFileDataSave:
    """
    Test the aspects of LocalFileDataSave class
    """

    def test_queue_get(self, mocker):
        """
        Ensure LocalFileDataSave calls queue get
        """
        queue = QueueStub()
        get_spy = mocker.spy(queue, "get")
        sut = make_sut()
        sut.save(queue)
        get_spy.assert_called_once()

    def test_save_call(self, mocker):
        """
        Ensure LocalFileDataSave calls save with correct value
        """
        save_data_repo = SaveDataRepoStub()
        save_spy = mocker.spy(save_data_repo, "save")
        sut = make_sut(save_data_repo=save_data_repo)
        sut.save(QueueStub())
        save_spy.assert_called_once_with(dict(valid="data"))
