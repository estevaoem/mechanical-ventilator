# pylint:disable=no-self-use
from .....src.data.protocols.serial_reader import SerialReader
from .....src.data.usecases.data_fetcher.serial_data_fetcher import \
    SerialDataFetcher
from .....src.utils.queue import Queue


class QueueStub(Queue):
    """
    A Queue Stub class
    """

    def get(self) -> dict:
        """
        Gets a dict item from the queue
        """
        return dict(valid="data")

    def put(self, item: dict) -> None:
        """
        Puts a dict item into the queue
        """


class SerialReaderStub(SerialReader):
    """
    A SerialReader Stub class
    """

    def read_line(self) -> str:
        """
        Reads line of incomming serial stream
        """
        return """{
                    "pressure": 1.3,
                    "flow": 1.3
                }"""

    def send_ack(self, message: str) -> None:
        """
        Sends ack of received message
        """


def make_sut(serial_reader: SerialReader = SerialReaderStub()) -> SerialDataFetcher:
    """
    Creates the system under test object
    """
    return SerialDataFetcher(serial_reader=serial_reader)


class TestSerialDataFetcher:
    """
    Test the aspects of SerialDataFetcher class
    """

    def test_fetch_serial_read(self, mocker):
        """
        Ensure SerialDataFetcher calls read_line once
        """
        serial_reader = SerialReaderStub()
        read_line_spy = mocker.spy(serial_reader, "read_line")
        sut = make_sut(serial_reader=serial_reader)
        sut.fetch(QueueStub())
        read_line_spy.assert_called_once()

    def test_send_ack_call(self, mocker):
        """
        Ensure SerialDataFetcher calls send_ack if ack is true with correct value
        """
        serial_reader = SerialReaderStub()
        send_ack_spy = mocker.spy(serial_reader, "send_ack")
        sut = make_sut(serial_reader=serial_reader)
        sut.fetch(QueueStub(), ack=True)
        message = serial_reader.read_line()
        send_ack_spy.assert_called_once_with(message)

    def test_fetch_queue_put(self, mocker):
        """
        Ensure SerialDataFetcher puts correct data on queue
        """
        queue = QueueStub()
        put_spy = mocker.spy(queue, "put")
        sut = make_sut()
        sut.fetch(queue)
        put_spy.assert_called_once_with({"pressure": 1.3, "flow": 1.3})
