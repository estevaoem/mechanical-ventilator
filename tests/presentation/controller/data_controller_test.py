# pylint:disable=no-self-use
# pylint:disable=dangerous-default-value

from typing import Any, Callable, Iterable, Optional

from ....src.domain.usecases.data_fetcher import DataFetcher
from ....src.domain.usecases.data_saver import DataSaver
from ....src.presentation.controller.data_controller import DataController
from ....src.utils.queue import Queue
from ....src.utils.thread import Thread


class ThreadStub(Thread):
    """
    A Thread Stub class
    """

    def build(
        self,
        target: Optional[Callable[..., Any]],
        args: Iterable[Any] = (),
        daemon: Optional[bool] = True,
        keep_alive: Optional[bool] = False,
    ) -> None:
        """
        Builds the thead-like operation
        """

    def start(self) -> None:
        """
        Starts the thead-like operation
        """

    def join(self, timeout: Optional[float] = None) -> None:
        """
        Awaits the thead-like operation
        """

    def _keep_alive_wrapper(self, *args) -> None:
        """
        Wrapps incomming target to keep it alive
        """


class QueueStub(Queue):
    """
    A Queue Stub class
    """

    def put(self, item: dict) -> None:
        """
        Puts a dict item into the queue
        """

    def get(self) -> dict:
        """
        Gets a dict item from the queue
        """


class DataFetcherStub(DataFetcher):
    """
    A DataFetcher Stub class
    """

    def fetch(self, queue: Queue, ack: Optional[bool] = False) -> None:
        """
        Fetches data from source and puts it into a queue
        """


class DataSaverStub(DataSaver):
    """
    A DataSaver Stub class
    """

    def save(self, queue: Queue) -> None:
        """
        Saves data from queue
        """


def make_sut(
    data_fetcher: DataFetcher = DataFetcherStub(),
    data_saver: DataSaver = DataSaverStub(),
    data_saver_thread: Thread = ThreadStub(),
    queue: Queue = QueueStub(),
    is_running=False,
):
    """
    Creates the system under test object
    """
    return DataController(
        data_fetcher=data_fetcher,
        data_saver=data_saver,
        data_saver_thread=data_saver_thread,
        queue=queue,
        is_running=is_running,
    )


class TestDataController:
    """
    Test the aspects of DataController class
    """

    def test_data_saver_thread_build_call(self, mocker):
        """
        Ensure DataController calls data_saver_thread build with correct values
        """
        data_saver = DataSaverStub()
        queue = QueueStub()
        data_saver_thread = ThreadStub()
        build_spy = mocker.spy(data_saver_thread, "build")
        sut = make_sut(
            data_saver=data_saver, queue=queue, data_saver_thread=data_saver_thread
        )
        sut.handle()
        build_spy.assert_called_once_with(
            target=data_saver.save, args=[queue], keep_alive=True
        )

    def test_data_saver_thread_start_call(self, mocker):
        """
        Ensure DataController calls data_saver_thread start once
        """
        data_saver_thread = ThreadStub()
        start_spy = mocker.spy(data_saver_thread, "start")
        sut = make_sut(data_saver_thread=data_saver_thread)
        sut.handle()
        start_spy.assert_called_once()

    def test_data_fetcher_call(self, mocker):
        """
        Ensure DataController calls data fetcher with correct value
        """

        def fetch_stub(queue: Queue) -> None:
            """
            A fetch stub method
            """
            queue.get()
            sut._is_running = False  # pylint:disable=protected-access

        queue = QueueStub()

        data_fetcher = DataFetcherStub()
        data_fetcher.fetch = fetch_stub
        fetch_spy = mocker.spy(data_fetcher, "fetch")

        sut = make_sut(queue=queue, is_running=True, data_fetcher=data_fetcher)
        sut.handle()
        fetch_spy.assert_called_once_with(queue)
