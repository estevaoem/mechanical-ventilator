from typing import Optional


class Queue:
    """
    Queue interface class
    """

    def __init__(self, max_size: Optional[int] = None):
        self._max_size = max_size

    def put(self, item: dict) -> None:
        """
        Puts a dict item into the queue
        """
        raise NotImplementedError

    def get(self) -> dict:
        """
        Gets a dict item from the queue
        """
        raise NotImplementedError
