from ..common.model import Model


class Updater:
    """
    Updates a section of the aplication
    """

    def update(self, data: Model) -> None:
        """
        Controls what is received on class constructor
        """
