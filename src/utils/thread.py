from typing import Any, Callable, Iterable, Optional


class Thread:
    """
    Thread interface class
    """

    def __init__(self):
        self._thread = None
        self._target = None
        self._args = None
        self._daemon = None
        self._keep_alive = None

    def build(
        self,
        target: Optional[Callable[..., Any]],
        args: Iterable[Any] = (),
        daemon: Optional[bool] = True,
        keep_alive: Optional[bool] = False,
    ) -> None:
        """
        Builds the thead-like object
        """
        raise NotImplementedError

    def start(self) -> None:
        """
        Starts the thead-like operation
        """
        raise NotImplementedError

    def join(self, timeout: Optional[float] = None) -> None:
        """
        Awaits the thead-like operation
        """
        raise NotImplementedError

    def _keep_alive_wrapper(self, *args) -> None:
        """
        Wrapps incomming target to keep it alive
        """
        raise NotImplementedError
