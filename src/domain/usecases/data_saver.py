from ...utils.queue import Queue


class DataSaver:
    """
    Saves data
    """

    def save(self, queue: Queue) -> None:
        """
        Saves data from queue
        """
