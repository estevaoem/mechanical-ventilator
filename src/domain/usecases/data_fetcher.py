from typing import Optional

from ...utils.queue import Queue


class DataFetcher:
    """
    Fetches data
    """

    def fetch(self, queue: Queue, ack: Optional[bool] = False) -> None:
        """
        Fetches data from source and puts it into a queue.
        If ack is True, returns an ack package to the source
        """
