from ...common.model import Model


class DataModel(Model):
    """
    DataModel data model
    """

    pressure: float
    flow: float
    volume: float
    vt: float
    vm: float
    tpl: int
    ppeak: float
    ppl: float
    peep: float
    qpico: float
