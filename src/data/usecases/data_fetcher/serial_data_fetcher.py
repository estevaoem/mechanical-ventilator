from ast import literal_eval
from typing import Optional

from ....common.typing import validate_typing
from ....domain.usecases.data_fetcher import DataFetcher
from ....utils.queue import Queue
from ...protocols.serial_reader import SerialReader


class SerialDataFetcher(DataFetcher):
    """
    Adapts data comming from a serial source to a Model
    """

    @validate_typing
    def __init__(self, serial_reader: SerialReader):
        self._serial_reader = serial_reader

    @validate_typing
    def fetch(self, queue: Queue, ack: Optional[bool] = False) -> None:
        line = self._serial_reader.read_line()
        if ack:
            self._serial_reader.send_ack(line)

        data = literal_eval(line)
        queue.put(data)
