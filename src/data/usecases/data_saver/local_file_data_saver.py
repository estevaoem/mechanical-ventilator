from ....common.typing import validate_typing
from ....domain.usecases.data_saver import DataSaver
from ....utils.queue import Queue
from ...protocols.save_data_repo import SaveDataRepo


class LocalFileDataSaver(DataSaver):
    """
    Saves data to a local file
    """

    @validate_typing
    def __init__(self, save_data_repo: SaveDataRepo):
        self.save_data_repo = save_data_repo

    @validate_typing
    def save(self, queue: Queue) -> None:
        """
        Saves data
        """
        data = queue.get()
        self.save_data_repo.save(data)
