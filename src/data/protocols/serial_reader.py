class SerialReader:
    """
    SerialReader interface class
    """

    def __init__(self):
        self._serial = None

    def read_line(self) -> str:
        """
        Reads line of incomming serial stream
        """
        raise NotImplementedError

    def send_ack(self, message: str) -> None:
        """
        Sends ack of received message
        """
        raise NotImplementedError
