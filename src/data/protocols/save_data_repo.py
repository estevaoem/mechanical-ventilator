from typing import Any


class SaveDataRepo:
    """
    Save data through a repository
    """

    def save(self, data: Any) -> None:
        """
        Saves data
        """
