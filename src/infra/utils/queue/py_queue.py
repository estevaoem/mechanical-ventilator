from queue import Queue as QueueInfra

from ....common.typing import validate_typing
from ....utils.queue import Queue


class PyQueue(Queue):
    """
    Queue implementation with python Queue
    """

    def __init__(self, queue: QueueInfra = QueueInfra()):
        self._queue = queue
        super().__init__()

    @validate_typing
    def put(self, item: dict) -> None:
        """
        Puts a dict item into the queue
        """
        self._queue.put(item)

    def get(self) -> dict:
        """
        Gets a dict item from the queue
        """
        return self._queue.get()
