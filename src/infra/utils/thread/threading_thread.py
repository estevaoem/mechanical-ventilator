from threading import Thread as SingleCoreThread
from typing import Any, Callable, Iterable, Optional

from ....common.typing import validate_typing
from ....utils.thread import Thread


class ThreadingThread(Thread):
    """
    Implements Thread class infrastructure
    """

    def __init__(self, thread=SingleCoreThread):
        self._Thread = thread  # pylint:disable=invalid-name
        super().__init__()

    @validate_typing
    def build(
        self,
        target: Optional[Callable[..., Any]],
        args: Iterable[Any] = (),
        daemon: Optional[bool] = True,
        keep_alive: Optional[bool] = False,
    ) -> None:
        """
        Builds the thread object
        """
        self._target = target
        self._args = args
        self._daemon = daemon
        self._keep_alive = keep_alive
        self._thread = self._Thread(
            target=self._keep_alive_wrapper, args=self._args, daemon=self._daemon
        )

    def start(self) -> None:
        """
        Starts the thread
        """
        self._thread.start()

    def join(self, timeout: Optional[float] = None) -> None:
        """
        Awaits the thead-like operation
        """
        self._thread.join(timeout=timeout)

    def _keep_alive_wrapper(self, *args) -> None:
        """
        Wrapps incomming target to keep it alive
        """
        self._target(*args)
        while self._keep_alive:
            self._target(*args)
