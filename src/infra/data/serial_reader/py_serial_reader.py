from serial import Serial

from ....common import env
from ....data.protocols.serial_reader import SerialReader


class PySerialReader(SerialReader):
    """
    Uses PySerial package to implement SerialReader interface
    """

    def __init__(self, pyserial=Serial):
        self._Serial = pyserial  # pylint:disable=invalid-name
        super().__init__()
        self._build()

    def _build(self) -> None:
        """
        Builds the serial object
        """
        self._serial = self._Serial(
            port=env["SERIAL_PORT"], baudrate=env["SERIAL_BAUDRATE"]
        )

    def read_line(self) -> str:
        """
        Reads line of incomming serial stream
        """
        message = self._serial.readline()
        decoded_message = message.decode()
        return decoded_message[:-1]

    def send_ack(self, message: str) -> None:
        """
        Sends ack of received message
        """
        ack_message = f"{str(dict(ack=message))}\n"
        self._serial.write(ack_message.encode())
