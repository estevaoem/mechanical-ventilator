from time import time
from typing import Any

from ....data.protocols.save_data_repo import SaveDataRepo


class TxtSaveDataRepo(SaveDataRepo):
    """
    Saves each received data on a txt file
    """

    def save(self, data: Any) -> None:
        """
        Saves data on a txt file
        """
        with open("logs.txt", mode="a", encoding="utf-8") as file:
            file.write(f"{time()}\t{data}\n")
