from pydantic import validate_arguments as _validate_arguments

# https://pydantic-docs.helpmanual.io/usage/validation_decorator/#custom-config
validate_typing = _validate_arguments(config=dict(arbitrary_types_allowed=True))
