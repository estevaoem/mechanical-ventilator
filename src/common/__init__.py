from os import getenv

with open(file="port.txt", mode="r", encoding="utf-8") as file:
    port = file.read()

env = {
    "SERIAL_BAUDRATE": getenv("SERIAL_BAUDRATE") or "9600",
    "SERIAL_PORT": getenv("SERIAL_PORT") or port,
}
