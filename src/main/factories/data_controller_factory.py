from ...data.usecases.data_fetcher.serial_data_fetcher import SerialDataFetcher
from ...data.usecases.data_saver.local_file_data_saver import \
    LocalFileDataSaver
from ...infra.data.save_data_repo.txt_save_data_repo import TxtSaveDataRepo
from ...infra.data.serial_reader.py_serial_reader import PySerialReader
from ...infra.utils.queue.py_queue import PyQueue
from ...infra.utils.thread.threading_thread import ThreadingThread
from ...presentation.controller.data_controller import DataController


def make_data_controller() -> DataController:
    """
    Assembles the DataController object
    """
    queue = PyQueue()
    thread = ThreadingThread()
    save_data_repo = TxtSaveDataRepo()
    data_saver = LocalFileDataSaver(save_data_repo)
    serial_reader = PySerialReader()
    data_fetcher = SerialDataFetcher(serial_reader)
    return DataController(data_fetcher, data_saver, thread, queue)
