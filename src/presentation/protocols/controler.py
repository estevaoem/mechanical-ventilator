class Controller:
    """
    Controls a portion of the code
    """

    def handle(self) -> None:
        """
        Controls what is received on class constructor
        """
