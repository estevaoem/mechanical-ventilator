from ...common.typing import validate_typing
from ...domain.usecases.data_fetcher import DataFetcher
from ...domain.usecases.data_saver import DataSaver
from ...utils.queue import Queue
from ...utils.thread import Thread
from ..protocols.controler import Controller


class DataController(Controller):
    """
    Controls application data flow
    """

    @validate_typing
    def __init__(
        self,
        data_fetcher: DataFetcher,
        data_saver: DataSaver,
        data_saver_thread: Thread,
        queue: Queue,
        is_running: bool = True,
    ):
        self._data_fetcher = data_fetcher
        self._data_saver = data_saver
        self._data_saver_thread = data_saver_thread
        self._queue = queue
        self._is_running = is_running

    def handle(self) -> None:
        """
        Handles received data to multiple sections of the application
        """
        self._data_saver_thread.build(
            target=self._data_saver.save, args=[self._queue], keep_alive=True
        )
        self._data_saver_thread.start()
        while self._is_running:
            self._data_fetcher.fetch(self._queue)
